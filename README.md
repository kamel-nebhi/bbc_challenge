# BBC Challenge report

This process allows a first EDA of BBC website audience dataset. 

## Data preparation

First we need to concatenate all files:
```sh
cat *_00 all_00
```

Then we need to use the python pre-processing.py script:
```python
import pandas as pd
import sqlite3

# init chunk size
chunksize = 300000

i=0

# create and connect to the DB
con = sqlite3.connect("BBC_eda_utf8.db") # database file input
cur = con.cursor()
# specify the UTF-8 encoding for the DB
cur.executescript("""
    PRAGMA encoding="UTF-8";
""")
# create the table
cur.executescript("""
  DROP TABLE IF EXISTS bbc_audience;
  CREATE TABLE bbc_audience (id VARCHAR(255), dateandtime DATETIME DEFAULT CURRENT_TIMESTAMP, search_term TEXT, platform VARCHAR(255), app_type VARCHAR(255), product VARCHAR(255), name TEXT, pageurl VARCHAR(2083), region TEXT);
""")
con.commit()

# split the merge dataset into chunk of 300000 to avoid any memory issue and append to sqlite DB
for chunk in pd.read_csv("../data/all_00",sep='|',chunksize=chunksize, parse_dates=[1], infer_datetime_format=True, error_bad_lines=False, names=['id', 'dateandtime', 'search_term', 'platform', 'app_type', 'product', 'name', 'pageurl', 'region']):
    chunk.to_sql('bbc_audience', con, if_exists='append', index=False)
    i += 1
    print("chunk ", i, " append to table bbc_audience")
```


## Data analysis

Connect to sqlite3 and database:
```sh
sqlite3
.open /home/nebhi/various_proj/bbc/BBC_eda.db
```

Query news product vs audience order by date:
```sql
SELECT strftime('%Y-%m-%d', dateandtime) as date, count(*) as cnt FROM bbc_audience WHERE product = 'news' GROUP BY strftime('%Y-%m-%d', dateandtime) ORDER BY date ASC;
```

Rank platform importance:
```sql
SELECT platform, COUNT(*) AS cnt FROM bbc_audience GROUP BY platform order by cnt DESC;
```

Platform vs. product importance:
```sql
SELECT product, platform, COUNT(platform)  FROM bbc_audience GROUP BY product, platform;
```

Search term vs. product importance:
```sql
SELECT product, lower(search_term), COUNT(search_term) as cnt  FROM bbc_audience GROUP BY product, lower(search_term) ORDER by cnt DESC LIMIT 60;\
```

These request are a sample.

This is also possible to request the DB directly through python:
```python
    cur.execute("SELECT product, platform, COUNT(platform) FROM bbc_audience GROUP BY product, platform")
    print(cur.fetchall())
```

## DataViz

The data-visualization is performed by Tableau Desktop, exploiting directly the results of the different queries.

These charts are integrated in the report.pdf file.



