import pandas as pd
import sqlite3

# init chunk size
chunksize = 300000

i=0

# create and connect to the DB
con = sqlite3.connect("BBC_eda_utf8.db") # database file input
cur = con.cursor()
# specify the UTF-8 encoding for the DB
cur.executescript("""
    PRAGMA encoding="UTF-8";
""")
# create the table
cur.executescript("""
  DROP TABLE IF EXISTS bbc_audience;
  CREATE TABLE bbc_audience (id VARCHAR(255), dateandtime DATETIME DEFAULT CURRENT_TIMESTAMP, search_term TEXT, platform VARCHAR(255), app_type VARCHAR(255), product VARCHAR(255), name TEXT, pageurl VARCHAR(2083), region TEXT);
""")
con.commit()

# split the merge dataset into chunk of 300000 to avoid any memory issue and append to sqlite DB
for chunk in pd.read_csv("../data/all_00",sep='|',chunksize=chunksize, parse_dates=[1], infer_datetime_format=True, error_bad_lines=False, names=['id', 'dateandtime', 'search_term', 'platform', 'app_type', 'product', 'name', 'pageurl', 'region']):
    chunk.to_sql('bbc_audience', con, if_exists='append', index=False)
    i += 1
    print("chunk ", i, " append to table bbc_audience")
	